$(window).on('load',function(){
    $('.loader-main').addClass('loader-inactive');
    //Adding Meta Tags
    $('head').prepend('<meta name="theme-color" content="#000000">');
    $('head').append('<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">');
    $('head').append('<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" href="app/splash/iphonexs.png"> ')
    $('head').append('<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" href="app/splash/iphonexsmax.png">')
    $('head').append('<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3)" href="app/splash/iphoneplus.png"> ')
    $('head').append('<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" href="app/splash/iphoneregular.png">')
});

$(document).ready(function(){      
    
    var isPWA=true;
    $("#main").css('display','block');

    //Activate the PWA    
    if(isPWA === true){
        var loadJS = function(url, implementationCode, location){
            var scriptTag = document.createElement('script');
            scriptTag.src = url;
            scriptTag.onload = implementationCode;
            scriptTag.onreadystatechange = implementationCode;
            location.appendChild(scriptTag);
        };
        function loadPWA(){}
        loadJS('js/pwa.js', loadPWA, document.body);
    }
    
}); 