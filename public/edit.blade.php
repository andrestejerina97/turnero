@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($services)
                <h4 class="title">Editar Servicio</h4>
                @else
                <h4 class="title">Nuevo Servicio</h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal" method="post" onsubmit="save(event)"  id="form_service" action="{{route('admin.store.service')}}"
                    role="form">
                    @csrf
                    @isset($services)
                    @foreach ($services as $service)
            <input type="hidden" name="id" value="{{$service->id}}">
                    @include("admin.partials.form-service")
    
                    @endforeach
                    @else
                 @include("admin.partials.form-service")

                @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                         
                        @isset($services)
                        <button type="submit" class="btn btn-primary">Actualizar Servicio</button>
                        @else
                        <button type="submit" class="btn btn-primary">Guardar Servicio</button>
                    
                        @endisset
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.mask.min.js')}}" type="text/javascript"></script>

<script>
$(document).ready(function(){
  $("#duration").inputmask("99-99");
  $("#duration").inputmask();
});

    function save(e) {
        e.preventDefault();
        var formdata = new FormData($("#form_service")[0]);
            $.ajax({
                url         : "{{route('admin.store.service')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Servicio guardado con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        let url="{{route('admin.edit.service',['id'=>0])}}"
                        url=url.replace('0',data.result);
                        location.href=url;
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    }
</script>
@endsection