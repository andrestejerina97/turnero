<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteUserProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('admin')->middleware('auth')->group(function(){
  //reservaciones
  Route::get('/estadisticas','ChartController@index')->name('admin.index.chart');
  Route::get('/mi-estadisticas-actual/{id?}/{date?}','ChartController@edit')->name('admin.edit.chart');
  Route::post('/guardar-estadisticas-/{id?}','ChartController@store')->name('admin.store.chart');
  Route::post('/actualizar-estadisticas/{id?}','ChartController@update')->name('admin.update.chart');
  Route::get('/eliminar-estadisticas/{id?}','ChartController@delete')->name('admin.delete.chart');
  Route::post('/estadisticas-filtro','ChartController@search')->name('admin.search.chart');
  Route::get('/filtro-estadisticas','ChartController@search')->name('admin.search.chart');

});



