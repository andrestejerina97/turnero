<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteUserProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('admin')->middleware('auth')->group(function(){
  //reservaciones
  Route::get('/caja','BoxController@index')->name('admin.index.box');
  Route::get('/mi-caja-actual/{id?}/{date?}','BoxController@edit')->name('admin.edit.box');
  Route::post('/guardar-caja-/{id?}','BoxController@store')->name('admin.store.box');
  Route::post('/actualizar-caja/{id?}','BoxController@update')->name('admin.update.box');
  Route::get('/eliminar-caja/{id?}','BoxController@delete')->name('admin.delete.box');
  Route::post('/caja-filtro','BoxController@search')->name('admin.search.box');
  Route::get('/filtro-caja','BoxController@search')->name('admin.search.box');

});



