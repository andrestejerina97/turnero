<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class service extends Model
{
    protected $table="services";
    protected $guarded=["id","created_at","modified_at"];
    use SoftDeletes;
    public function reservations()
    {
        return $this->hasMany('App\reservation');
    }
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeName($query,$param)
    {
        return $query->where('name','LIKE','%'.$param.'%');
    }
    public function scopeAmount($query,$param)
    {
        return $query->where('amount','LIKE','%'.$param.'%');
    }
}
