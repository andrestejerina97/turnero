<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class box extends Model
{
    protected $guarded=["id","created_at","modified_at"];


    public function invoices()
    {
        return $this->hasMany('App\invoice');
    }
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeCreated($query,$date)
    {
        return $query->where('date',$date);
    }
}
