<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class reservation extends Model
{
    use SoftDeletes;

    protected $table="reservations";
    protected $guarded=["id","created_at","modified_at"];

    public function services()
    {
        return $this->belongsTo('App\service','service_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function employees()
    {
        return $this->belongsTo('App\employee','employee_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function pacients()
    {
        return $this->belongsTo('App\pacient','pacient_id','id')->withDefault([
            'name' => 'Eliminado',
        ]);
    }
    public function statuses()
    {
        return $this->belongsTo('App\status','status_id','id');
    }
    
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }

    public function scopeTitle($query,$param)
    {
        return $query->where('Title','LIKE','%'.$param.'%');
    }
    public function scopeDate($query,$param)
    {
        if ($param != null) {
            return $query->where('date_at',$param);
        }
    }
    public function scopeService($query,$param)
    {
        if ($param != null) {
            return $query->where('service_id',$param);
        }
    }
    public function scopePacient($query,$param)
    {
        if ($param != null) {
            return $query->where('pacient_id',$param);
        }
    }
    public function scopeEmployee($query,$param)
    {
        if ($param != null) {
            return $query->where('employee_id',$param);
        }
    }
    public function scopeNoneStatus($query,$param)
    {
        if ($param != null and $param=="Anulado") {
            return $query->where('status_id','!=',191);
        }
    }
    
}
