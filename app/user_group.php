<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_group extends Model
{
    
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
}
