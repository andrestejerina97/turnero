<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class employee extends Model
{
    use SoftDeletes;

    protected $table="employees";
    protected $guarded=["id","created_at","modified_at"];

    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeLastname($query,$param)
    {
        if ($param != '') {
            return $query->where('lastname','LIKE','%'.$param.'%');
        }
    }
    public function scopePhone($query,$param)
    {
        return $query->where('phone','LIKE','%'.$param.'%');
    }
    public function scopeName($query,$param)
    {
        if ($param != '') {
            return $query->where('name','LIKE','%'.$param.'%');
        }
    }
}
