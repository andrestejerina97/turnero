<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoice extends Model
{
    protected $guarded = ["id", "created_at", "modified_at"];

    public function reservation()
    {
        return $this->belongsTo('App\reservation', 'reservation_id')->withDefault([
            'service' => [
                'amount' => "Reserva borrada"
            ]
        ]);
    }
    public function user()
    {
        return $this->belongsTo('App\user', 'created_by');
    }
    public function payment()
    {
        return $this->belongsTo('App\pay_method', 'payment_id');
    }
    public function getIsCardPaymentAttribute()
    {
        if ($this->payment_id == 4 || $this->payment_id == 5) {
            return true;
        } else {
            return false;
        }
    }
    public function getIsCashPaymentAttribute()
    {
        return $this->payment_id == 3 ? true : false;
    }
}
