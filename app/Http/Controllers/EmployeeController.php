<?php

namespace App\Http\Controllers;

use App\employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
     $employees=employee::orderBy('name','DESC')->all();
 
     return view('admin.employees.index',compact('employees'));
    }
    public function edit($id=0)
    {
     if($id==0){
         $employees=employee::orderBy('name','DESC')->all();
 
         return view('admin.employees.edit');   
       }else{
        $employee=employee::where("id",'=',$id);
       return view('admin.publicaciones.edit-case',compact('employee'));   
       }
    }
    public function store($id=0,Request $request)
    {
        $data=$request->except('_token','id');
        if($id==0){
         $employee=employee::create($data);
         return response()->json(['result'=>$employee->id]);   
       }else{
        $employee=employee::where("id",'=',$id)->update($data);
 
        return response()->json(['result'=>$id]);   
     }
    }
}
