<?php

namespace App\Http\Controllers;

use App\reservation;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function index()
    {
     $reservations=reservation::orderBy('name','DESC')->all();
     return view('admin.reservations.index',compact('reservations'));
    }
    public function edit($id=0)
    {
     if($id==0){
         $reservations=reservation::orderBy('name','DESC')->all();
 
         return view('admin.reservations.edit');   
       }else{
        $reservation=reservation::where("id",'=',$id);
       return view('admin.publicaciones.edit-case',compact('reservation'));   
       }
    }
    public function store($id=0,Request $request)
    {
        $data=$request->except('_token','id');
        if($id==0){
         $reservation=reservation::create($data);
         return response()->json(['result'=>$reservation->id]);   
       }else{
        $reservation=reservation::where("id",'=',$id)->update($data);
 
        return response()->json(['result'=>$id]);   
     }
    }

}
