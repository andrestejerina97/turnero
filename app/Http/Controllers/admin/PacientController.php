<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\pacient;
use App\reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PacientController extends Controller
{
  public function index()
  {

    $pacients = pacient::Customer(Auth::user()->customer_id)->orderBy('id', 'desc')->get();
    return view('admin.pacients.index', compact('pacients'));
  }
  public function edit($pacient_id = 0)
  {

    if ($pacient_id == 0) {
      return view('admin.pacients.edit');
    } else {

      $pacients = pacient::where('pacients.id', '=', $pacient_id)->get();
      return view('admin.pacients.edit')
        ->with('pacients', $pacients);
    }
  }

  public function store(Request $request)
  {
    $data = $request->except('_token', 'id');

    if ($request->has('id')) {
      /*  $validatedData = $request->validate([
                'email' => ['required', Rule::unique('pacients')->ignore($request->input('id'))]
              ]);*/

      $pacient = pacient::where("id", '=', $request->input('id'))->update($data);

      return response()->json(['result' => $request->input('id')]);
    } else {
      /*  $validatedData = $request->validate([
                'email' => ['required','unique:pacients'],
              ]);*/
      $data['customer_id'] = Auth::user()->customer_id;

      $pacient = pacient::create($data);
      return response()->json(['result' => $pacient->id]);
    }
  }
  public function search(Request $request)
  {
    $datas = $request->except('_token');
    $pacients = pacient::Customer(Auth::user()->customer_id)
      ->Lastname($datas['lastname'])
      ->Name($datas['name'])
      ->orderBy('id', 'DESC')->get();
    return view('admin.pacients.index', compact('pacients'));
  }

  public function delete($id)
  {
    if ($id != 'id') {
      $user = pacient::find($id);
      $user->delete();
      return response()->json(['result' => 1]);
    } else {
      return response()->json(['result' => -1]);
    }
  }

  public function show($pacient_id)
  {
    $customer_id = Auth::user()->customer_id;

    $reservations = reservation::Customer(Auth::user()->customer_id)
      ->Service(null)
      ->Pacient($pacient_id)
      ->with(['services',"statuses","pacients","employees"])
      ->orderBy('id', 'DESC')->paginate(20);

    return view('admin.pacients.show', compact('reservations'));
  }
}
