<?php

namespace App\Http\Controllers\admin;

use App\employee;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees=employee::Customer(Auth::user()->customer_id)->orderBy('id','desc')->get();
        return view('admin.employees.index',compact('employees'));
    }
    public function edit($employee_id=0)
    {
    
        if ($employee_id==0) {
        return view('admin.employees.edit');  
          
        }else{

        $employees=employee::where('employees.id','=',$employee_id)->get();
      return view('admin.employees.edit')
      ->with('employees',$employees);
        }
      
     }
    
     public function store(Request $request)
     {
         $data=$request->except('_token','id');
 
         if($request->has('id')){
   
              
         $employee=employee::where("id",'=',$request->input('id'))->update($data);
  
         return response()->json(['result'=>$request->input('id')]);
        }else{
          $data['is_active']=1;
          $data['customer_id']=Auth::user()->customer_id;
          $employee=employee::create($data);
          return response()->json(['result'=>$employee->id]); 
               
      }
     }
     public function delete($id){
        if ($id != 'id') {
          $user= employee::find($id);
          $user->delete();
          return response()->json(['result'=>1]);
        }else{
          return response()->json(['result'=>-1]);
  
        }
      }
      public function search(Request $request)
      {
          $datas=$request->except('_token');
          $employees= employee::Customer(Auth::user()->customer_id) 
          ->Lastname($datas['lastname'])
          ->Name($datas['name'])
          ->orderBy('id','DESC')->get();
        return view('admin.employees.index',compact('employees'));
      
     }
}
