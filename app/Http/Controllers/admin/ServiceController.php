<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    public function index()
    {
     $services=service::Customer(Auth::user()->customer_id)->orderBy('name','asc')->get();
 
     return view('admin.services.index',compact('services'));
    }
    public function edit($service_id=0)
    {
    
        if ($service_id==0) {
        return view('admin.services.edit');  
          
        }else{

        $services=service::where('services.id','=',$service_id)->get();
      return view('admin.services.edit')
      ->with('services',$services);
        }
      
     }
    
     public function store(Request $request)
     {
         $data=$request->except('_token','id');
 
         if($request->has('id')){
   
          if($request->has('web')){
            $data['web']=1;

          }else{
            $data['web']=0;
          }
         $service=service::where("id",'=',$request->input('id'))->update($data);
  
         return response()->json(['result'=>$request->input('id')]);
        }else{
          $data['customer_id']=Auth::user()->customer_id;
          if($request->has('web')){
            $data['web']=1;

          }else{
            $data['web']=0;
          }
          $service=service::create($data);
          return response()->json(['result'=>$service->id]); 
               
      }
     }
     public function delete($id){
        if ($id != 'id') {
          $user= service::find($id);
          $user->delete();
          return response()->json(['result'=>1]);
        }else{
          return response()->json(['result'=>-1]);
        }
      }
      public function search(Request $request)
      {
          $datas=$request->except('_token');
          $services= service::Customer(Auth::user()->customer_id) 
          ->Name($datas['name'])
          ->Amount($datas['amount'])
          ->orderBy('id','DESC')->get();
        return view('admin.services.index',compact('services'));
      
     }
}
