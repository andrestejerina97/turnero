<?php

namespace App\Http\Controllers\admin;

use App\box;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ChartController extends Controller
{
    public function index()
    {

        return view('admin.chart.index');
    }
    public function edit($box_id=0)
    {
    
        if ($box_id==0) {
        return view('admin.boxs.edit');  
          
        }else{

        $boxs=box::where('boxs.id','=',$box_id)->get();
      return view('admin.boxs.edit')
      ->with('boxs',$boxs);
        }
      
     }
    
     public function store(Request $request)
     {
         $data=$request->except('_token','id');
 
         if($request->has('id')){
   
              
         $box=box::where("id",'=',$request->input('id'))->update($data);
  
         return response()->json(['result'=>$request->input('id')]);
        }else{
          $data['is_active']=1;
          $data['customer_id']=Auth::user()->customer_id;
          $box=box::create($data);
          return response()->json(['result'=>$box->id]); 
               
      }
     }
     public function delete($id){
        if ($id != 'id') {
          $user= box::find($id);
          $user->delete();
          return response()->json(['result'=>1]);
        }else{
          return response()->json(['result'=>-1]);
  
        }
      }
      public function search(Request $request)
      {
          $datas=$request->except('_token');
          $boxs= box::Customer(Auth::user()->customer_id) 
          ->Lastname($datas['lastname'])
          ->Name($datas['name'])
          ->orderBy('id','DESC')->get();
        return view('admin.boxs.index',compact('boxs'));
      
     }
}
