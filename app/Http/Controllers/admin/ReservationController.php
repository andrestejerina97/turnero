<?php

namespace App\Http\Controllers\admin;

use App\box;
use App\employee;
use App\Http\Controllers\Controller;
use App\invoice;
use App\invoice_detail;
use App\pacient;
use App\pay_method;
use App\reservation;
use App\service;
use App\status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ReservationController extends Controller
{
  public function index()
  {
    $customer_id = Auth::user()->customer_id;
    $employees = employee::Customer($customer_id)->orderBy('lastname', 'asc')->get();
    $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
    $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
    $reservations = reservation::Customer($customer_id)->with('services')->with('pacients')->with('employees')->orderBy(DB::raw('DATE(date_at)', 'time_at'), 'DESC')->paginate(10);
    return view('admin.reservations.index', compact('reservations', 'pacients', 'services', 'employees'));
  }
  public function edit($reservation_id = 0, $date = 0)
  {
    $customer_id = Auth::user()->customer_id;

    if ($reservation_id == 0) {

      $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
      $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
      $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
      $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();
      $payments = pay_method::Customer($customer_id)->Enabled()->orderBy('id', 'desc')->get();

      if ($date == 0) {
        $date = date("Y-m-d");
      }
      // $reservations=reservation::orderBy('id','desc')->get();
      return view('admin.reservations.edit', compact('payments', 'statuses', 'pacients', 'services', 'employees'))
        ->with('date', $date);
    } else {
      $payments = pay_method::Customer($customer_id)->Enabled()->orderBy('id', 'desc')->get();
      $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
      $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
      $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
      $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();
      $reservations = reservation::with('services')->where('reservations.id', '=', $reservation_id)->get();
      return view('admin.reservations.edit', compact('payments', 'reservations', 'statuses', 'pacients', 'services', 'employees'));
    }
  }

  public function store(Request $request)
  {
    $data = $request->except('_token', 'id', 'phone', 'pay_method_id', 'invoice_total', 'generate_pay');
    if ($request->has('id')) {
      //validation reservation IMPORTANT!!
      //   $start = Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires');
      //   $end = Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires');
      $reservation_validates = reservation::where('date_at', $request->input('date_at'))
        ->where('employee_id', $data['employee_id'])
        ->select('time_end', 'time_at', 'id')
        ->get();
      $count = 0;
      foreach ($reservation_validates as $reservation_validate) {
        if ($reservation_validate->id != $request->input('id')) {
          $start = Carbon::createFromTimeString($reservation_validate['time_at'], 'America/Argentina/Buenos_Aires');
          $end = Carbon::createFromTimeString($reservation_validate['time_end'], 'America/Argentina/Buenos_Aires');
          if ($start->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
            $count = 1;
          } else {
            if ($end->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
              $count = 1;
            }
          }
        }
      }

      if ($count > 0) {
        return response()->json(['result' => -1, 'message' => "La fecha y hora de la cita se encuentra ocupada actualmente, por favor seleccione otro día u horario"]);
      }
      //end validation to reservation
      $data['customer_id'] = Auth::user()->customer_id;

      if (!is_numeric($request->pacient_id)) {
        $pacient = pacient::create([
          'name' => $request->pacient_id,
          'customer_id' => $data['customer_id'],
          'phone' => $request->input('phone', '')
        ]);
        $data['pacient_id'] = $pacient->id;
      } else {
        pacient::where('id', $request->pacient_id)->update(['phone' => $request->phone]);
      }
      $reservation = reservation::where("id", '=', $request->input('id'))->update($data);

      /**Validation payment status */
      if ($request->has("generate_pay")) {
        $statusPayed = status::where('name', 'Pagado')->where('customer_id', Auth::user()->customer_id)->first();
        if ($statusPayed && $statusPayed->id == $request->get('status_id')) {
          $box = box::where('date', date("Y-m-d"))
            ->where('active', 1)
            ->first();
          if (!$box) $box = box::create(['date' => date("Y-m-d"), 'active' => 1, 'customer_id' => Auth::user()->customer_id]);
          $service = service::find($request->input('service_id'));
          if (!$service) $service = "no definido";

          $invoice = invoice::create([
            'payment_id' => $request->get("pay_method_id", null),
            'total' => $request->get('invoice_total'),
            'partial_total' => $service->amount,
            'created_by' => Auth::user()->id,
            'modified_by' => Auth::user()->id,
            'customer_id' => Auth::user()->customer_id,
            'box_id' => $box->id,
            'reservation_id' => $request->input('id')
          ]);
          $invoice_detail = invoice_detail::create([
            'total' => $request->get('invoice_total'),
            'qty' => 1,
            'invoice_id' => $invoice->id,
          ]);
        }
      }
      return response()->json(['result' => $request->input('id')]);
    } else {
      //validation reservation IMPORTANT!!
      $start = Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires');
      $end = Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires');
      $reservation_validates = reservation::where('date_at', $request->input('date_at'))
        ->where('employee_id', $data['employee_id'])
        ->select('time_end', 'time_at')
        ->get();
      $count = 0;
      foreach ($reservation_validates as $reservation_validate) {
        $start = Carbon::createFromTimeString($reservation_validate['time_at'], 'America/Argentina/Buenos_Aires');
        $end = Carbon::createFromTimeString($reservation_validate['time_end'], 'America/Argentina/Buenos_Aires');
        if ($start->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
          $count = 1;
        } else {
          if ($end->between(Carbon::createFromTimeString($request->input('time_at'), 'America/Argentina/Buenos_Aires'), Carbon::createFromTimeString($request->input('time_end'), 'America/Argentina/Buenos_Aires'))) {
            $count = 1;
          }
        }
      }

      if ($count > 0) {
        return response()->json(['result' => -1, 'message' => "La fecha y hora de la cita se encuentra ocupada actualmente, por favor seleccione otro día u horario"]);
      }
      //end validation to reservation


      // validation new customer/patient
      $data['customer_id'] = Auth::user()->customer_id;

      if (!is_numeric($request->pacient_id)) {
        $pacient = pacient::create([
          'name' => $request->pacient_id,
          'customer_id' => $data['customer_id'],
          'phone' => $request->input('phone', '')
        ]);
        $data['pacient_id'] = $pacient->id;
      } else {
        pacient::where('id', $request->pacient_id)->update(['phone' => $request->phone]);
      }
      //End validation new customer/patient
      $reservation = reservation::create($data);
      /**Validation payment status */
      if ($request->has("generate_pay")) {
        $statusPayed = status::where('name', 'Pagado')->where('customer_id', Auth::user()->customer_id)->first();
        if ($statusPayed && $statusPayed->id == $request->get('status_id')) {
          $box = box::where('date', date("Y-m-d"))
            ->where('active', 1)
            ->first();
          if (!$box) $box = box::create(['date' => date("Y-m-d"), 'active' => 1, 'customer_id' => Auth::user()->customer_id]);
          $service = service::find($request->input('service_id'));
          if (!$service) $service = "no definido";
          $invoice = invoice::create([
            'payment_id' => $request->get("pay_method_id", null),
            'total' => $request->get('invoice_total'),
            'partial_total' => $service->amount,
            'created_by' => Auth::user()->id,
            'modified_by' => Auth::user()->id,
            'customer_id' => Auth::user()->customer_id,
            'box_id' => $box->id,
            'reservation_id' => $reservation->id
          ]);
          $invoice_detail = invoice_detail::create([
            'total' => $request->get('invoice_total'),
            'qty' => 1,
            'invoice_id' => $invoice->id,
          ]);
        }
      }
      return response()->json(['result' => $reservation->id]);
    }
  }

  public function delete($id)
  {
    if ($id != 'id') {
      $user = reservation::find($id);
      $user->delete();
      return response()->json(['result' => 1]);
    } else {
      return response()->json(['result' => -1]);
    }
  }

  public function search(Request $request)
  {
    $customer_id = Auth::user()->customer_id;
    $datas = $request->except('_token');

    $reservations = reservation::Customer(Auth::user()->customer_id)
      ->Service($datas['service_id'])
      ->Employee($datas['employee_id'])
      ->Date($datas['date_at'])
      ->orderBy('id', 'DESC')->get();

    $employees = employee::Customer($customer_id)->orderBy('id', 'desc')->get();
    $services = service::Customer($customer_id)->orderBy('id', 'desc')->get();
    $pacients = pacient::Customer($customer_id)->orderBy('id', 'desc')->get();
    $statuses = status::Customer($customer_id)->orderBy('id', 'desc')->get();
    return view('admin.reservations.index', compact('reservations', 'pacients', 'services', 'employees'));
  }
  public function show($modal=0,$pacient_id=null)
  {
    $customer_id = Auth::user()->customer_id;
    $reservations = reservation::Customer(Auth::user()->customer_id)
      ->Service(null)
      ->Pacient($pacient_id)
      ->with(['services',"statuses","pacients","employees"])
      ->orderBy('id', 'DESC')->paginate(10);
    
      if($modal==="1"){
        return view('admin.reservations.partials.byPacient', compact('reservations'));
      }

  }
}
