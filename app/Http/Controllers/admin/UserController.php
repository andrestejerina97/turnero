<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index()
    {   
      $customer_id=Auth::user()->customer_id;
        $users=User::Customer($customer_id)->orderBy('id','desc')->orderBy('id','DESC')->get();
        return view('admin.users.index')
        ->with('users',$users);
    }
    public function edit($user_id=0)
    {
    
        if ($user_id==0) {
        return view('admin.users.edit');  
          
        }else{

        $users=User::with('roles')->where('id','=',$user_id)->get();
      return view('admin.users.edit')
      ->with('users',$users);
        }
      
     }
     public function search(Request $request)
     {
         $datas=$request->except('_token');
         $customer_id=Auth::user()->customer_id;
         $users= User::Customer($customer_id) 
         ->Name($datas['name'])
         ->UserName($datas['username'])
         ->orderBy('id','DESC')->get();
       return view('admin.users.index',compact('users'));
     
    }
    public function searchuser($id,Request $request)
    {
      $users=User::join('users','users.users_id','users.id')
      ->where('publications_id','=',$id)
      ->where($request->input('filter'),'LIKE','%'.$request->input('search')."%")->orderBy('users.id','DESC')
      ->with('users')->with('publications')
      ->get();
      return view('admin.users.index')
      ->with('users',$users)
      ->with('publications_id',$id);
    }
    public function update(Request $request)
    {
      $data= $request->except('_token','photo','role','password_confirmation','password');

      if ($request->input('password')=="") {
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
          'role' => ['required','min:3'],
  
        ]);

      }else{
 
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role' => ['required','min:3'],
  
        ]);
        $data['password']=Hash::make($request->input('password'));
      }

        
          $user=User::where('id','=',$request->input('id'))->update($data);
          return response()->json(['result'=>$request->input('id')]);

    }
 
    public function store(Request $request)
    {

      if($request->has('id')){
        $data= $request->except('_token','photo','password_confirmation','password');

        if ($request->has('password') && $request->input('password') == '') {
          $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
          ],
          [
            'email.unique' => "El email de acceso ya está en uso,por favor introduzca uno nuevo",
          ]
        );

 

      }else{
        
          $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required',Rule::unique('users')->ignore($request->input('id'))],
            'password' => ['required', 'string', 'confirmed'],
          ],
          [
            'email.unique' => "El email de acceso ya está en uso,por favor introduzca uno nuevo",
          ]
        );
        $data['password']=Hash::make($request->input('password'));  

        }
        if ($request->has('is_active')) {
          $data['is_active']=1;
        }else{
          $data['is_active']=0;
        }
        if ($request->has('is_admin')) {
          $data['is_admin']=1;
          $user=User::find($request->input('id'));
          $user->syncRoles("admin");
        }else{
          $data['is_admin']=0;
          $user=User::find($request->input('id'));
          $user->syncRoles("user");

        }
          $user=User::where('id','=',$request->input('id'))->update($data);

    
          return response()->json(['result'=>$request->input('id')]);

   
      }else{
        $validatedData = $request->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required','unique:users'],
          'password' => ['required', 'string', 'confirmed'],
        ],
        [
          'email.unique' => "El email de acceso ya está en uso,por favor introduzca uno nuevo",
          ]
      );
        $data= $request->except('_token','photo','role');
        $data['password']=Hash::make($request->input('password'));
        $data['customer_id']=Auth::user()->customer_id;

        if ($request->has('is_active')) {
          $data['is_active']=1;
        }else{
          $data['is_active']=0;
        }
        if ($request->has('is_admin')) {
          $data['is_admin']=1;
          $user=User::create($data);
          $user->assignRole("admin");
        }else{
          $data['is_admin']=0;
          $user=User::create($data);
          $user->assignRole("user");
  
        }
      }

      return response()->json(['result'=>$user->id]);

    }
    public function delete($id){
      if ($id != 'id') {
        $user= User::find($id);
        $user->delete();
        return response()->json(['result'=>1]);
      }else{
        return response()->json(['result'=>-1]);

      }
    }
}
