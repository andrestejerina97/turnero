<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pay_method extends Model
{
    public function scopeCustomer($query,$customer_id)
    {
        return $query->where('customer_id',$customer_id);
    }
    public function scopeEnabled($query)
    {
        return $query->where('status',1);
    }
}
