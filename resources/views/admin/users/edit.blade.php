@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($users)
                <h4 class="title">Editar Usuario</h4>
                @else
                <h4 class="title">Nuevo Usuario</h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal" method="post" onsubmit="save(event)"  id="form_user" action="{{route('admin.store.user')}}"
                    role="form">
                    @csrf
                    @isset($users)
                    @foreach ($users as $user)
            <input type="hidden" name="id" value="{{$user->id}}">
                    @include("admin.partials.form-user")
    
                    @endforeach
                    @else
                 @include("admin.partials.form-user")

                @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                         
                        @isset($users)
                        <button type="submit" class="btn btn-primary">Actualizar Usuario</button>
                        @else
                        <button type="submit" class="btn btn-primary">Guardar Usuario</button>
                    
                        @endisset
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

    function save(e) {
        e.preventDefault();
        var formdata = new FormData($("#form_user")[0]);
            $.ajax({
                url         : "{{route('admin.store.user')}}",
                data        : formdata,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'POST',
                dataType:"JSON",
                beforeSend:function() {
                  swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                    
                  });
                },
                success: function(data, textStatus, jqXHR){
                    if (data.result != null) {
                      swal({
                        tittle:"Excelente!",
                        text:"Cambios guardados con éxito,será redirigido a continuación",
                        icon :"success",
                      }).then((value) => {
                        //let url="{{route('admin.edit.user',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href='{!! url()->previous()!!}';//url;
                      });
                    }else{
                      swal("Ups!",data.message,"error");   
                    }

                  },
                  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  },
                  statusCode:{
                    422:function(data) {

                      console.log(data.responseJSON.errors);
                      let lista= "";
                for(var k in data.responseJSON.errors) {
                lista += ""+ data.responseJSON.errors[k][0] +"";              
                }
                
              lista+="";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error: '+lista,
                      icon: 'error',
                    });                    }
                  }
            });
    }
</script>
@endsection