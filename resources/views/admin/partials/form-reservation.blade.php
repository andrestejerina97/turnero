
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Servicio</label>
    <div class="col-lg-10">
      <select id="service_id"  name="service_id" onchange="ChangeDuration();TimeReservetionEnd()" class="select2 form-control" required>
        <option value="">-- SELECCIONE --</option>
        @foreach ($services as $service)
        <option data-duration="{{$service->duration}}" data-amount="{{$service->amount}}" value="{{$service->id}}"
         @isset($reservation->service_id)
        @if ($reservation->service_id == $service->id)
            selected
        @endif
        @endisset
          >{{$service->name}}</option>         
        @endforeach
          
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Cliente</label>

    <div class="col-lg-10" >
      <select id="pacient_id" name="pacient_id" class="select2 form-control" style="width: 100%" required>
        <option value="">-- SELECCIONE --</option>
        @foreach ($pacients as $pacient)
        <option data-phone="{{$pacient->phone}}"  value="{{$pacient->id}}"
         @isset($reservation->pacient_id)
        @if ($reservation->pacient_id == $pacient->id)
            selected
        @endif
        @endisset
          >{{$pacient->name}} {{$pacient->lastname}}</option>         
        @endforeach
      </select>
    </div>
</div>
<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Historial de cliente:</label>
  <div class="col-lg-10" >
    <a href="javascript:;" id="btn_show_pacient" class=""><i class="fa fa-eye"></i> Ver</a>
  </div>
</div>
<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 control-label">Telefono:</label>

  <div class="col-lg-10" >
      <input type="text" name="phone" value="@isset($reservation->pacient->phone){{$reservation->pacient->phone}}@endisset"  class="form-control" id="phone" placeholder="Teléfono(opcional)">
  </div>
</div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Encargado:</label>
    <div class="col-lg-10">
      <select name="employee_id" class="form-control" required>
        <option value="">-- SELECCIONE --</option>
        @foreach ($employees as $employee)
        <option  value="{{$employee->id}}"
         @isset($reservation->employee_id)
        @if ($reservation->employee_id == $employee->id)
            selected
        @endif
        @endisset
          >{{$employee->name}}</option>         
        @endforeach
          
      </select>
    </div>
  </div>




  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Fecha turno</label>
    <div class="col-lg-4">
      <input type="date" name="date_at" value="@isset($reservation->date_at){{date('Y-m-d', strtotime($reservation->date_at))}}@else{{date('Y-m-d', strtotime($date))}}@endisset"  required class="form-control" placeholder="Fecha">
    </div>
    <label for="inputEmail1" class="col-lg-2 control-label">Hora turno</label>

    <div class="col-lg-4">
      <input type="time" name="time_at" value="@isset($reservation->time_at){{date('H:i', strtotime($reservation->time_at))}}@endisset"  required class="form-control" id="time_at" placeholder="Hora" onchange="TimeReservetionEnd()">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Duración</label>
    <div class="col-md-5">
      <input type="time" id="duration" value="@isset($reservation->services->duration){{date('H:i',strtotime($reservation->services->duration))}}@endisset" name="duration" disabled class="form-control" placeholder="Hora" onchange="TimeReservetionEnd()">
    </div>
  </div>
  <div class="form-group">
      <label for="time_end" class="col-lg-2 control-label">Hora de finalización</label>
      <div class="col-lg-5">
        <input type="time" name="time_end" value="@isset($reservation->time_end){{date('H:i', strtotime($reservation->time_end))}}@endisset" required class="form-control" id="time_end" placeholder="Hora">
      </div>
    </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nota</label>
    <div class="col-lg-4">
    <textarea class="form-control" name="note" placeholder="Nota">@isset($reservation->date_at){{$reservation->note}} @endisset</textarea>
    </div>
  </div>
 <!-- <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Asunto</label>
    <div class="col-lg-10">
      <input type="text" name="title" value="" required class="form-control" id="inputEmail1" placeholder="Asunto">
    </div>
  </div> -->
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Estado de la cita</label>
    <div class="col-lg-4">
      <select name="status_id" id="status_id" class="form-control" required onchange="isApplicate()">
        @foreach ($statuses as $statuse)
        <option value="{{$statuse->id}}"
        @isset($reservation->status_id)
        @if ($reservation->status_id == $statuse->id)
            selected
        @endif
        @else
        @if($statuse->name=="Pendiente")
        selected
        @endif
        @endisset
        >{{$statuse->name}}</option>
        @endforeach
      </select>
    </div>
  </div>

  <div id="invoice" >
      <h4>Datos del pago:</h4>
    <div class="form-group">
      <label for="inputEmail1" class="col-lg-2">Total pagado €:</label>
      <div class="col-lg-10" >
          <input type="text" name="invoice_total" value="@isset($reservation->pacient->phone){{$reservation->pacient->phone}}@endisset"  class="form-control" id="invoice_total" placeholder="Total pagado">
      </div>
    </div>
    <div class="form-group">
      <label for="inputEmail1" class="col-lg-2 ">Forma de pago</label>
      <div class="col-lg-4">
        <select name="pay_method_id" id="pay_method_id" class="form-control" required onchange="isApplicate()">
          @foreach ($payments as $payment)
          <option value="{{$payment->id}}"
          @isset($reservation->status_id)
          @if ($reservation->payment_id == $payment->id)
              selected
          @endif
          @else
          @if($payment->name_method=="Efectivo")
          selected
          @endif
          @endisset
          >{{$payment->name_method}}</option>
          @endforeach
        </select>
      </div>
    </div>


    <div class="form-group">
      <label for="inputEmail1" class="col-lg-2 control-label" ></label>
      <div class="col-md-6 col-lg-6">
      <label  >
         Generar pago
        <input type="checkbox" name="generate_pay" id="generate_pay" value="1" checked> 
      </label>
  
      </div>
    </div>
  

  </div>