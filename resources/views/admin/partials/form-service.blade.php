<div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Nombre*</label>
    <div class="col-md-6">
      <input type="text" name="name" value="@isset($service) {{$service->name}} @endisset" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Monto*</label>
    <div class="col-md-6">
      <input type="number" name="amount" min="0" value="@isset($service){{$service->amount}}@endisset"  placeholder="€XX.XX" step="0.01" class="form-control" id="name" placeholder="Nombre">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Duración(hh:mm)*</label>
    <div class="col-md-6">
      <input type="tel"  id="duration" name="duration" value="@isset($service){{date('H:i',strtotime($service->duration))}}@endisset" required class="form-control duration" id="inputEmail1" placeholder="Horas:Minutos">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Visible en app</label>
    <div class="col-md-6">
      <input type="checkbox" name="web" value="1" @isset($service)@if($service->web)checked @endif @endisset> 
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail1" class="col-lg-2 control-label">Color:</label>
    <div class="col-md-6">
      <input type="color" name="color" class="form-control" value="@isset($service){{$service->color}}@endisset" id="color" placeholder="color..">
    </div>
  </div>
