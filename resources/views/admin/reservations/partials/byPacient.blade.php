<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4>Mi Cliente</h4>

            </div>
            <div class="card-content table-responsive">

                @if (count($reservations)>0)
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Profesional</th>
                        <th>Servicio</th>
                        <th>Fecha</th>
                        <th>Hora Inicio</th>
                        <th>Hora Finalización</th>
                        <th>Estado</th>
                    </thead>
                    @foreach($reservations as $reservation)
                    <tr>
                        <td>{{$reservation->employees->name." ".$reservation->employees->lastname}}</td>
                        <td>{{$reservation->services->name}}</td>
                        <td>{{$reservation->date_at}}</td>
                        <td>{{$reservation->time_at}}</td>
                        <td>{{$reservation->time_end}}</td>
                        <td>{{$reservation->statuses->name}}</td>
                    </tr>
                    @endforeach
                    @else
                    <p class='alert alert-danger'>No hay turnos</p>
                    @endif
                </table>
            </div>
        </div>
    </div>
    <div class="form-group pull-right">
        {{ $reservations->links("admin.reservations.partials.paginate") }}

    </div>

</div>
