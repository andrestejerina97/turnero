@extends('layouts.admin')
@section('css')
<link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />

@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                @isset($reservations)
                <h4 class="title">Editar Turno</h4>
                @else
                <h4 class="title">Nuevo Turno</h4>

                @endisset
            </div>
            <div class="card-content table-responsive">
                <form class="form-horizontal" method="post" onsubmit="save(event)" id="form_reservation"
                    action="{{route('admin.update.reservation')}}" role="form">
                    @csrf
                    @isset($reservations)
                    @foreach ($reservations as $reservation)
                    <input type="hidden" name="id" value="{{$reservation->id}}">
                    @include("admin.partials.form-reservation")
                    @endforeach

                    @else

                    @include("admin.partials.form-reservation")
                    @endisset
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">

                            @isset($reservations)
                            <button type="submit" class="btn btn-primary" id="btn-save-reservation">Actualizar
                                Turno</button>
                            @else
                            <button type="submit" class="btn btn-primary" id="btn-save-reservation">Guardar
                                Turno</button>

                            @endisset
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/es.js')}}" type="text/javascript"></script>

<script>
    checkedReservationStatus();
    $(document).on('change', '#status_id', function () {
        checkedReservationStatus();
    });
    $('.select2').select2({
        language: "es",
        tags: true,

    });

    function checkedReservationServices() {
        $("#invoice_total").val($("#service_id option:selected").data("amount"))
    }

    function checkedReservationStatus() {
        if ($("#status_id option:selected").text() === "Pagado") {
            $("#invoice").removeClass("hidden")
            $("#pay_method_id").attr("required", 'required')
            $("#invoice_total").attr("required", 'required')
            checkedReservationServices()
            $("#generate_pay").prop('checked', true)
        } else {
            $("#invoice").addClass("hidden")
            $("#pay_method_id").attr("required", false)
            $("#invoice_total").attr("required", false)
            $("#invoice_total").val("")
            $("#generate_pay").prop('checked', false)

        }
    }
    /*   function search() {
        let filter = $("#input_filter").val();
        i = 1;
    $('#pacient_id > option').each(function() {
    //  alert($(this).text())
         $this = $(this);
            $this.removeAttr('selected');
            if ($this.text().indexOf(filter) != -1) {
                $this.show();
                if(i == 1){
                    $this.attr('selected', 'selected');
                }
                i++;
            } else {
                $this.hide();
            }
     // $('select').val(filter);
    });
    }
*/

    function save(e) {
        e.preventDefault();
        var formdata = new FormData($("#form_reservation")[0]);
        $.ajax({
            url: "{{route('admin.store.reservation')}}",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,

                });
            },
            success: function (data, textStatus, jqXHR) {
                if (data.result != -1) {
                    swal({
                        tittle: "Excelente!",
                        text: "Guardado con éxito,será redirigido a continuación",
                        icon: "success",
                    }).then((value) => {
                        //let url="{{route('admin.edit.reservation',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href = '{!! url()->previous()!!}'; //url;
                    });
                } else {
                    swal("Ups!", data.message, "error");
                }

            },
            error: function (data, message, res) {
                let lista = "<ul>";
                for (var k in data.responseJSON.errors) {
                    lista += "<li>" + data.responseJSON.errors[k][0] + "</li>";
                }

                lista += "</ul>";
                let au = '<div class="alert alert-danger msj" role="alert">' + lista + '</div>';

                swal({
                    title: 'Ups!',
                    text: 'Hubo un error',
                    html: lista,
                    type: 'error',
                });

                // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
            },
            statusCode: {
                422: function (data) {

                    console.log(data.responseJSON.errors);
                    let lista = "";
                    for (var k in data.responseJSON.errors) {
                        lista += "" + data.responseJSON.errors[k][0] + "";
                    }

                    lista += "";
                    let au = '<div class="alert alert-danger msj" role="alert">' + lista + '</div>';

                    swal({
                        title: 'Ups!',
                        text: 'Hubo un error: ' + lista,
                        icon: 'error',
                    });
                }
            }
        });
    }
    $('#phone').val($('#pacient_id option:selected').data('phone'));


    $('#pacient_id').change(function () {

        $('#phone').val($('#pacient_id option:selected').data('phone'));
    });
    $('#btn_show_pacient').click(function () {

        openModalCustomer();
    });


    function openModalCustomer() {
        let url = "{{route('admin.show.reservation',['modal'=>1,'pacient_id'=>-1])}}";
        let pacientId = $('#pacient_id option:selected').val();
        url = url.replace("-1", pacientId)
     
        $.ajax({
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            type: 'get',
            beforeSend: function () {
                swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,
                });
            },
            success: function (data, textStatus, jqXHR) {
                swal.close();
                $("#modal_customer_historial_body").html(data);
                $("#modal_customer_historial").modal("show")

            },
            error: function (data, message, res) {
                swal({
                    title: 'Ups!',
                    text: 'Hubo un error',
                    html: "Revise los datos seleccionados",
                    type: 'error',
                });
            },
        });

    }

    function openModalCustomerPaginate(url){
        $.ajax({
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            type: 'get',
            beforeSend: function () {
                $("#modal_customer_historial_body").html("Cargando...");
            },
            success: function (data, textStatus, jqXHR) {
                $("#modal_customer_historial_body").html(data);
            },
            error: function (data, message, res) {
                swal({
                    title: 'Ups!',
                    text: 'Hubo un error',
                    html: "Revise los datos seleccionados",
                    type: 'error',
                });
            },
        });
    }

   
</script>
@endsection
