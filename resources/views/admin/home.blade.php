@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
  		<div class="card-header" id="top">
      	<h4 class="title" id="encabezados">Mi agenda online</h4>
  		</div>
  		<div class="card-content table-responsive" >
				<div id="calendar"></div>
			</div>
		</div>
	</div>
</div>
 
@endsection
@section('scripts')
<script>
	$(document).ready(function() {

		$('#calendar').fullCalendar({
			lang: 'es',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '<?php echo date('Y-m-d');?>',
			editable: false,
			minTime:"08:00",
			hiddenDays:[0],
			eventLimit: true, // allow "more" link when too many events
			events: <?php echo json_encode($thejson); ?>
		});
		
	});

</script>
@endsection