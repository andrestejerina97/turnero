@extends('layouts.admin')
@section('content')
<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Caja del día {{$date}}</h4>
  </div>
  <div class="card-content table-responsive">
	
	<form method="get" action="{{route('admin.search.box')}}" role="form"  class="form-horizontal">
    @csrf
    <input type="hidden" name="view" value="boxs">
		<div class="form-group">
			<div class="col-lg-3">
				<input type="date" name="date" placeholder="Fecha" class="form-control">           				
			</div>
			<div class="col-lg-1">
				
			</div>
			<div class="col-lg-2">
				<button class="btn " >Buscar</button>
			</div>

			
		</div>
	</form>
  @php
  $total_cash=0;
  $total_card=0;
  @endphp
  @if ($boxs && count($boxs->invoices)>0)
  @php
      $invoices=$boxs->invoices;

  @endphp
  <table class="table table-bordered table-hover">
    <thead>
    <th>Usuario</th>
    <th>Fecha</th>
    <th>Pago</th>
    <th>Total parcial</th>
    <th>Descuento</th>
    <th>Total final</th>
    <th></th>
    </thead>
  
          @foreach($invoices as $invoice)
          <tr>
          <td>{{$invoice->user->name}}</td>
          <td>{{$invoice->created_at}}</td>
          <td>{{$invoice->payment->name_method}}</td>
          <td>€{{$invoice->partial_total}}</td>
          <td>{{$invoice->discount}}</td>
          <td>€ {{$invoice->total}}</td>
            @php
            if($invoice->getIsCashPaymentAttribute()){
              $total_cash=(float) $invoice->partial_total + $total_cash;
            }else if($invoice->getIsCardPaymentAttribute()){
              $total_card=(float) $invoice->total + $total_card;
            }

            @endphp
          <td style="width:280px;">
          <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
          <a href="{{route('admin.edit.reservation',$invoice->reservation_id)}}" class="btn btn-warning btn-xs">Ver reservacion</a>
          </td>
          </tr>
          @endforeach
          @else
          <p class='alert alert-danger'>No hay registros</p>
          @endif
    </table>
    <div class="form-horizontal text-center "><br>
      <table class="table table-bordered table-hover">
        <tr>
          <th><h4 class="title" >TOTAL EFECTIVO:  </h4></th>
          <th>€ {{$total_cash}}</th>
        </tr>
        <tr>
          <th><h4 class="title" >TOTAL TARJETA:   </h4></th>
        <th>€{{$total_card}}</th>
        </tr>
        <tr>
          <th><h4 class="title" >CAJA TOTAL:   </h4></th>
          <th>€{{$total_card + $total_cash}}</th>
        </tr>
      </table>
      

    </div>
			</div>
			</div>
	</div>
</div>
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

function eliminar(a) {
          swal({
            icon: "warning",
              text: '¿Seguro que deseas eliminar este Encargado?,se borrarán todos los datos relacionados con él,esto no se  puede revertir!".',
              buttons: {
                  cancel: "No,cancelar!",
                  catch: {
                    text: "Sí,eliminar",
                    value: "catch",
                  },
                },
              })
              .then((value) => {
                switch (value) {
                  case "catch":
               let id_box=$(a).data("id_box");
               let url="{{route('admin.delete.box','id')}}"
               url=url.replace("id",id_box);
                $.ajax({
                url         : url,
                cache       : false,
                contentType : false,
                processData : false,
                type        : 'get',
                dataType:"JSON",
                success     : function(data, textStatus, jqXHR){
                    if (data.result != -1) {
                      swal({
                        tittle:"Excelente!",
                        text:"Encargado eliminado con éxito",
                        icon :"success",
                      }).then((value) => {
                        location.href='{{route("admin.index.box")}}';


                      });

                    }else{

                    }
               
                  },
				  error:function(data,message,res){
                    let lista= "<ul>";
                for(var k in data.responseJSON.errors) {
                lista += "<li>"+ data.responseJSON.errors[k][0] +"</li>";              
                }
                
              lista+="</ul>";
              let au='<div class="alert alert-danger msj" role="alert">'+lista+'</div>';

                      swal({
                      title: 'Ups!',
                      text: 'Hubo un error',
                      html: lista,
                      type: 'error',
                    });

                   // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
                  }
            });
                    break;
                  default:
                  
                }
              });
        

      }   

</script>
@endsection