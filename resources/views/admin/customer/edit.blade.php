@extends('layouts.admin')
@section('content')
@if (Auth::user()->customer_id==1)

<div class="row">
	<div class="col-md-12">
<div class="btn-group pull-right">
<!--<div class="btn-group pull-right">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-download"></i> Descargar <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
  </ul>
</div>
-->
</div>
<div class="card">
  <div class="card-header" id="top">
      <h4 class="title" id="encabezados">Clientes</h4>
  </div>
  <div class="card-content table-responsive">

			
			<table class="table table-bordered table-hover">
			<thead>
      <th>Nombre completo</th>
      <th>Ciudad</th>
			<th>Clave </th>
      <th>Fecha de alta</th>
      <th>Usuarios</th>
			<th></th>
            </thead>
            @if (count($customers)>0)
                
			@foreach($customers as $customer)
				
				<tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->city}}</td>
                <td>{{$customer->keyname}}</td>
                <td>{{$customer->created_at}}</td>
                <td>
                  <ol>
                    @if (count($customer->users)>0)

                    @foreach ($customer->users as $user)
                  <li>{{$user->name}} -- {{$user->email}}</li>
                    @endforeach
                    @endif
                  </ol>
                </td>

        <td style="width:280px;">

              
				</td>
				</tr>
            @endforeach
            @else
            <p class='alert alert-danger'>No hay registros</p>
            @endif

			</table>
			</div>
			</div>
	</div>
</div>


<div class="row">
    <div class="card">
        @if (count($customers)>0)
                
@foreach($customers as $customer)
<div class="card">
    <form class="form-horizontal" method="post"   id="form_user" action="{{route('root.update')}}"
    role="form" enctype="multipart/form-data">
    @csrf
   
 @include("admin.partials.form-customer")

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10">
         
        @isset($customers)
        <button type="submit" class="btn btn-primary">Actualizar Usuario</button>
        @else
        <button type="submit" class="btn btn-primary">Guardar Usuario</button>
    
        @endisset
        </div>
      </div>
</form>
</div>
   
@endforeach
@else
<p class='alert alert-danger'>No hay registros</p>
@endif
      
</div>
    
@endif
@endsection


@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>

</script>
@endsection