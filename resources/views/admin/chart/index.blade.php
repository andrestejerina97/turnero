@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="btn-group pull-right">
                <!--<div class="btn-group pull-right">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-download"></i> Descargar <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
        <li><a href="report/clients-word.php">Word 2007 (.docx)</a></li>
      </ul>
    </div>
    -->
            </div>
            <div class="card">
                <div class="card-header" id="top">
                    <h4 class="title" id="encabezados">Estadísticas</h4>
                </div>
                <div class="card-content table-responsive">

                    <form method="get" action="{{ route('admin.search.box') }}" role="form" class="form-horizontal">
                        @csrf
                        <input type="hidden" name="view" value="boxs">
                        <div class="form-group">
                            <div class="col-lg-3">
                                <input type="date" name="date" placeholder="Fecha" class="form-control">
                            </div>
                            <div class="col-lg-1">

                            </div>
                            <div class="col-lg-2">
                                <button class="btn ">Buscar</button>
                            </div>


                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
              <div class="card-content table-responsive">
                <div id="chart_payment" style="width:100%; height:400px;"></div>
                <div id="chart_reservation" style="width:100%; height:400px;"></div>
                <div id="chart_reservation_employees" style="width:100%; height:400px;"></div>

              </div>
          </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="{{ asset('js/highcharts.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/highcharts-export.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/highcharts-more.min.js') }}" type="text/javascript"></script>

    <script>
      Highcharts.chart('chart_reservation_employees', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Reservas - personal'
    },
    subtitle: {
        text: 'Cantidad de reservas aceptadas'
    },
    xAxis: {
        categories: [
            'Ene',
            'Feb',
            'Mar',
            'Abr',
            'May',
            'Jun',
            'Jul',
            'Ago',
            'Sep',
            'Oct',
            'Nov',
            'Dic'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Rainfall (mm)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Tokyo',
        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

    }, {
        name: 'New York',
        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

    }, {
        name: 'London',
        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

    }, {
        name: 'Berlin',
        data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

    }]
});
Highcharts.chart('chart_reservation', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Reservas'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Aceptadas',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Pagadas',
            y: 11.84
        }, {
            name: 'Canceladas',
            y: 10.85
        }, {
            name: 'Anuladas',
            y: 4.67
        }]
    }]
});
    </script>
@endsection
