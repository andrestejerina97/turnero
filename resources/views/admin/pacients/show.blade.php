@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4>Historial de cliente</h4>

            </div>
            <div class="card-content table-responsive">

                @if (count($reservations)>0)
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>Profesional</th>
                        <th>Servicio</th>
                        <th>Fecha</th>
                        <th>Hora Inicio</th>
                        <th>Hora Finalización</th>
                        <th>Estado</th>

                        <th></th>
                    </thead>
                    @foreach($reservations as $reservation)
                    <tr>
                        <td>{{$reservation->employees->name." ".$reservation->employees->lastname}}</td>
                        <td>{{$reservation->services->name}}</td>
                        <td>{{$reservation->date_at}}</td>
                        <td>{{$reservation->time_at}}</td>
                        <td>{{$reservation->time_end}}</td>
                        <td>{{$reservation->statuses->name}}</td>

                        <td style="width:280px;">
                            <!--<a href="" class="btn btn-default btn-xs">Historial</a>-->
                            <a href="{{route('admin.edit.reservation',['reservation_id'=>$reservation->id,'date'=>0])}}"
                                class="btn btn-warning btn-xs">Editar</a>
                         
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <p class='alert alert-danger'>No hay turnos</p>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
<div class="form-group pull-right">
    {{ $reservations->links() }}
  </div>
@endsection
@section('scripts')
<script src="{{asset('js/sweetalert.min.js')}}" type="text/javascript"></script>

<script>
    function save(e) {
        e.preventDefault();
        var formdata = new FormData($("#form_pacient")[0]);
        $.ajax({
            url: "{{route('admin.store.pacient')}}",
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: "JSON",
            beforeSend: function () {
                swal("Por favor espera,tu petición se está procesando!", {
                    buttons: false,

                });
            },
            success: function (data, textStatus, jqXHR) {
                if (data.result != null) {
                    swal({
                        tittle: "Excelente!",
                        text: "Cliente guardado con éxito,será redirigido a continuación",
                        icon: "success",
                    }).then((value) => {
                        // let url="{{route('admin.edit.pacient',['id'=>0])}}"
                        //url=url.replace('0',data.result);
                        location.href = '{!! url()->previous()!!}'; //url;
                    });
                } else {
                    swal("Ups!", data.message, "error");
                }

            },
            error: function (data, message, res) {
                let lista = "<ul>";
                for (var k in data.responseJSON.errors) {
                    lista += "<li>" + data.responseJSON.errors[k][0] + "</li>";
                }

                lista += "</ul>";
                let au = '<div class="alert alert-danger msj" role="alert">' + lista + '</div>';

                swal({
                    title: 'Ups!',
                    text: 'Hubo un error',
                    html: lista,
                    type: 'error',
                });

                // swal("ups!","Hubo un error al procesar tu petición,vuelve a intentar por favor","error");
            },
            statusCode: {
                422: function (data) {

                    console.log(data.responseJSON.errors);
                    let lista = "";
                    for (var k in data.responseJSON.errors) {
                        lista += "" + data.responseJSON.errors[k][0] + "";
                    }

                    lista += "";
                    let au = '<div class="alert alert-danger msj" role="alert">' + lista + '</div>';

                    swal({
                        title: 'Ups!',
                        text: 'Hubo un error: ' + lista,
                        icon: 'error',
                    });
                }
            }
        });
    }

</script>
@endsection
