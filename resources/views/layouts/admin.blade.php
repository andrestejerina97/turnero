<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Evane</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="viewport" content="width=device-width" />
	  <meta name="description" content="Evane" /> 
    <meta name="author" content="Evane-LiderIt-QuodSystem"> 
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- CODELAB: Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-title" content="Wesafe">
    <meta name="theme-color" content="#ffffff"/>
    <meta name="description" content="Evane la mejor forma de administrar tu negocio">
	<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

  <link rel="apple-touch-icon" href="{{asset("app/icons/icon-152x152.png")}}">
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet"/>
<link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
<link rel="stylesheet" href="{{asset('customcss/estilistas.css')}}">

<style>
  #top {
    color:black; 
    font-family: elsie; 
    background-color:#316ceb;
}

#fondo{ 
    background-color: #FFFFFF;
}

#encabezados{
    color:rgb(255, 255, 255); 
    font-family: elsie;
    text-align: center;
}
#collapse:hover {
    background-color:#2950a3;
}


  .principal{
    background-color:#629fee !important;
    color: #ffffff !important;

}

.loader {
  position: fixed;
  right: 0;
  left: 0;
  top: 0;
  bottom: 0;
  margin: auto;
  width: 100vw;
  height: 100vh;
  z-index: 99999;
  background-color: #ffffff;
  opacity: 0.7;

}
		.whatsapp {
		  position:fixed;
		  width:50px;
		  height:50px;
		  bottom:30px;
		  right:30px;
		  background-color:#25d366;
		  color:#FFF;
		  border-radius:40px;
		  text-align:center;
		  font-size:25px;
		  z-index:100;
		}

		.whatsapp-icon {
		  margin-top:13px;
		}
</style>
@yield('css')
</head>

<body>
  <div id="loader"  class="loader">
    <div class="text-center" style="position: relative !important; top: 25vh; ">
      <a href="#" class="text-center"><img src="{{asset(Auth::user()->customers->logo)}}"  style="width: 40%; border-radius: 80px"></a>
      <h2 style="color:#000000">Cargando....</h2>

    </div>  
  </div>
  <div>
    
    {{-- <a href="https://wa.me/543516687683?text=Hola,%20tengo%20una%20consulta%20sobre%20Turnero" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a> --}}
        </div>
  <div class="wrapper">
    
      @include('layouts.sidebar.admin')
      <div class="main-panel" id="top">
      <nav class="navbar navbar-transparent navbar-absolute ">
        
        <div class="container-fluid">
          <div class="navbar-header">
   
            <div class="row">
              <button type="button" class="navbar-toggle" style="color:  #ffffff !important;" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                 
                <div style="position: fixed; left: 10%; padding:0px; margin:0px">
                <h3  style="color: #ffffff; "><i class="fa fa-user" aria-hidden="true" ></i>
                 @isset(Auth::user()->customers->name)
                 {{Auth::user()->customers->name}}
                 @endisset
                </h3>
                </div>
            </div>


          </div>
          <div class="collapse navbar-collapse" id="collapse">
          
          </div>
        </div>
      </nav>
    
      <div class="content" id="fondo">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
    </div>

  </div>
  <div class="modal"  id="modal_customer_historial" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Historial de cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modal_customer_historial_body">
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

</body>
<script src="{{asset('js/pwabuilder-sw.js')}}"></script>
<script  type="module"  src="{{asset('js/pwabuilder-sw-register.js')}}"></script>

  <!--   Core JS Files   -->
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/material.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/material-dashboard.js')}}"></script>

  <!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>

  <!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>


  <!-- Material Dashboard javascript methods -->

  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('js/demo.js')}}"></script>
  <script type="text/javascript">

      $(document).ready(function(){
        $("#loader").hide();
      });
     
    
 
      function HabilitarCantidad() {
        var x = document.getElementById("CajaCantidad");
        x.style.display = "block";
      }
      function ChangeCategory(){
        var x =document.getElementById("service_id");
        var y =document.getElementById("total_to_pay");
        var z =document.getElementById("total_to_paySave");
       y.value= x.options[x.selectedIndex].dataset.amount;
        
     // ApplyDiscount2();
      }
      function ChangeProduct(){
        var x =document.getElementById("product_id");
        var y =document.getElementById("total_to_pay");
        
        y.value= x.options[x.selectedIndex].dataset.cost;
      }
      function ChangeDuration(){
        var x =document.getElementById("service_id");
        var y =document.getElementById("duration");

        if(document.getElementById("invoice_total") != 'undefined'){
        var total =document.getElementById("invoice_total");
        total.value=x.options[x.selectedIndex].dataset.amount;
        }
        y.value= x.options[x.selectedIndex].dataset.duration;
      }

      function isApplicate(){
        var select = document.getElementById("status_id");
        var flag = document.getElementById("flag_buy_aplicated");
        var divBuyTreatment = document.getElementById("ventaAplicada");
        divBuyTreatment.style.display =  "none";
        
        if((select.value == 2) && (flag.value==0)) 
        {
          divBuyTreatment.style.display =  "block";
        }
        
      }
      function TimeReservetionEnd() {
        var duration =  document.getElementById("duration");
        var time_at = document.getElementById("time_at");
         
        if (duration != null && time_at != null) {
          let time_end = document.getElementById("time_end");

          let durationDate = duration.valueAsDate;
          let time_atDate = time_at.valueAsDate;
          
          let s = (durationDate.getTime() + time_atDate.getTime());
          
          let ms = s % 1000;
          s = (s - ms) / 1000;
          let secs = s % 60;
          s = (s - secs) / 60;
          let mins = s % 60;
          let hrs = (s - mins) / 60;

          time_end.value = leadZero(hrs) +":"+ leadZero(mins);
        }
        function leadZero(_something) {    
          if(parseInt(_something)<10) 
            return "0"+_something;
          return _something;//else    
        }
      }

      function ApplyDiscount(discount)
      {
        var paid_out = document.getElementById("paid_out");
        if (discount.name === "discount")
        {
          var total_to_pay = document.getElementById("total_to_pay");
          if (total_to_pay.value != null && discount.value != null) 
          {
            paid_out.value = total_to_pay.value - discount.value;
          }
        }
        if(discount.name === "total_to_pay")
        {
          var discountTag = document.getElementById("discount");
          discountTag.value = "0";
          if (discountTag != null && discount.value != null) {
            
            paid_out.value =discount.value - discountTag.value;
          }
        }
      }
        function ApplyDiscount2()
      {
        var paid_out = document.getElementById("paid_out");
        var discount = document.getElementById("discount");
        var total_to_pay = document.getElementById("total_to_pay");

        if (total_to_pay.value != null && discount.value != null) 
          {
            paid_out.value = total_to_pay.value - discount.value;
          }
       
      }

  </script>

  @yield('scripts')

  

</html>
