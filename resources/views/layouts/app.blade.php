<!doctype html>
<html lang="es">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

  <title>Evane web</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
  <meta name="description" content="Evane" /> 
  <meta name="author" content="Evane-LiderIt-QuodSystem"> 
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <!-- CODELAB: Add iOS meta tags and icons -->
  <meta name="apple-mobile-web-app-title" content="Wesafe">
  <meta name="theme-color" content="#ffffff"/>
  <meta name="description" content="Evane la mejor forma de administrar tu negocio">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/material-dashboard.css')}}" rel="stylesheet"/>
<link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>


<link rel="manifest" href="{{asset("manifest.json")}}" data-pwa-version="set_by_pwa.js">

<link rel="stylesheet" href="{{asset('customcss/estilistas.css')}}">
<style>
  #top {
    color:black; 
    font-family: elsie; 
    background-color:#316ceb;
}

#fondo{ 
    background-color: #FFFFFF;
}

#encabezados{
    color:rgb(255, 255, 255); 
    font-family: elsie;
    text-align: center;
}
#collapse:hover {
    background-color:#2950a3;
}


</style>
@yield('css')
</head>

<body>
    
  
          @yield('content')
     
 
</body>
<script src="{{asset('js/pwabuilder-sw.js')}}"></script>
<script  type="module"  src="{{asset('js/pwabuilder-sw-register.js')}}"></script>

  <!--   Core JS Files   -->
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

  <!--  Charts Plugin -->
<script src="{{asset('js/chartist.min.js')}}"></script>

  <!--  Notifications Plugin    -->
<script src="{{asset('js/bootstrap-notify.js')}}"></script>

  <!--  Google Maps Plugin    -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

  <!-- Material Dashboard javascript methods -->
<script src="{{asset('js/material-dashboard.js')}}"></script>

  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('js/demo.js')}}"></script>

  @yield('scripts')

  

</html>
