<div class="sidebar" id="top">
    <div class="logo">
        <a href="./" class="simple-text" id="encabezados">
            <img src="{{asset(Auth::user()->customers->logo)}}"
                style="background:transparent;border-radius: 30px; max-height: 40px; min-height: 40px; max-width: 40px; "
                loading="lazy" alt="">
            Evane
        </a>
    </div>

    <div class="sidebar-wrapper" id="fondo">
        <ul class="nav">
            <li>
                <a href="{{route('home')}}">
                    <i class="fa fa-home"></i>
                    <p>Agenda online</p>
                </a>
            </li>
            <li>
                <a href="{{route('admin.index.reservation')}}">
                    <i class="fa fa-calendar"></i>
                    <p>Turnos</p>
                </a>
            </li>

            @role('admin')
            <li>

                <a href="{{route('admin.index.box')}}">
                    <i class="fa fa-money"></i>
                    <p>Caja</p>
                </a>
            </li>
            <li>

                <a href="{{route('admin.index.pacient')}}">
                    <i class="fa fa-male"></i>
                    <p>Clientes</p>
                </a>
            </li>


            <!--por ahora innecesario  <li>
                    <a href="./?view=clinic-history">
                        <i class="fa fa-file"></i>
                        <p>Historias Clinicas</p>
                    </a>
                </li> -->


            <li>
                <a href="{{route('admin.index.employee')}}">
                    <i class="fa fa-support"></i>
                    <p>Encargados</p>
                </a>
            </li>

            <li>
                <a href="{{route('admin.index.service')}}">
                    <i class="fa fa-th-list"></i>
                    <p>Servicios</p>
                </a>
            </li>

            <li>
                <a href="{{route('admin.index.user')}}">
                    <i class="fa fa-users"></i>
                    <p>Usuarios</p>
                </a>
            </li>
            <li>
                <a href="{{route('admin.index.chart')}}">
                    <i class="fa fa-bar-chart-o"></i>
                    <p>Estadísticas</p>
                </a>
            </li>
            @endrole
            <li>
                <a href="javascript:;" onclick="document.getElementById('form_logout').submit()">
                    <form id="form_logout" method="POST" action="{{route('logout')}}">
                        @csrf
                        <i class="fa fa-close"></i>
                        <p>Cerrar sesión</p>
                    </form>
                </a>
            </li>
        </ul>
    </div>
</div>
