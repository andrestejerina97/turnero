<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('name',50)->nullable();
            $table->string('lastname',50)->nullable();
            $table->string('gender',50)->nullable();
            $table->date('day_of_birth')->nullable();
            $table->string('email',255)->nullable();;
            $table->string('address',255)->nullable();
            $table->string('phone',255)->nullable();
            $table->string('color',100)->nullable();
            $table->boolean('is_active')->defaul(true);
            $table->timestamps();
            $table->BigInteger('customer_id')->nullable()->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
