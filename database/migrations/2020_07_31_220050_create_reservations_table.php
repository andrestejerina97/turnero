<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->string('title',100)->nullable();
            $table->string('date_at',50)->nullable();
            $table->string('time_at',50)->nullable();
            #$table->datetime('created_at');
            $table->BigInteger('pacient_id')->unsigned()->nullable();
            $table->BigInteger('user_id')->nullable()->unsigned();
            $table->BigInteger('employee_id')->nullable()->unsigned();
            $table->boolean('is_web')->nullable(); 
            $table->BigInteger('status_id')->nullable()->unsigned();
            $table->BigInteger('service_id')->nullable()->unsigned();
            $table->text('note')->nullable(); 
            $table->string('time_end',50)->nullable();

            $table->foreign('pacient_id')->references('id')->on('pacients')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->BigInteger('customer_id')->nullable()->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
