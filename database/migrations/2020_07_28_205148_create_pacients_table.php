<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacients', function (Blueprint $table) {
            $table->id();
            $table->string ('no',50)->nullable();
            $table->string('name',50)->nullable();
            $table->string('lastname',50)->nullable();
            $table->string('gender')->nullable(); 
            $table->date('day_of_birth')->nullable();
            $table->string('addres',250)->nullable(); 
            $table->string('phone',250)->nullable(); 
            $table->string('alergy',500)->nullable(); 
            $table->boolean('is_favorite')->default(false);
            $table->string('is_active')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('provider',50)->nullable();

            
            $table->timestamps();
            $table->BigInteger('customer_id')->nullable()->unsigned();
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacients');
    }
}
