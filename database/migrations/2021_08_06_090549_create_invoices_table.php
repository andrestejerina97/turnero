<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('total')->nullable();
            $table->string('discount')->nullable();
            $table->string('tax')->nullable();
            $table->timestamps();
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->BigInteger('created_by')->nullable()->unsigned();
            $table->BigInteger('modified_by')->nullable()->unsigned();
            $table->BigInteger('box_id')->nullable()->unsigned();
            $table->BigInteger('customer_id')->nullable()->unsigned();
            $table->BigInteger('reservation_id')->nullable()->unsigned();
            $table->BigInteger('payment_id')->nullable()->unsigned();

            $table->foreign('reservation_id')->references('id')->on('reservations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modified_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('box_id')->references('id')->on('boxes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('payment_id')->references('id')->on('pay_methods')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
