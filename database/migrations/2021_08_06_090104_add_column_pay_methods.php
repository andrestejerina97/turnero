<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPayMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pay_methods', function (Blueprint $table) {
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('default')->nullable();
         
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('payment_id');
            $table->dropColumn('payment_id');
        });
        Schema::table('pay_methods', function (Blueprint $table) {
            $table->dropColumn('default');
            $table->dropColumn('status');
        });
    }
}
