<?php

use App\pay_method;
use Illuminate\Database\Seeder;

class PaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        pay_method::create([
            'name_method'=>"Tarjeta Crédito"
        ]);
        pay_method::create([
            'name_method'=>"Tarjeta"
        ]);
        pay_method::create([
            'name_method'=>"Efectivo"
        ]);
        pay_method::create([
            'name_method'=>"Tarjeta Débito"
        ]);
    }
}
