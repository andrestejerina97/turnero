<?php

namespace Database\Seeders;

use App\Models\offices;
use Illuminate\Database\Seeder;

class OfficesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       offices::create([
           'name'=> 'Nueva Córdoba',
           'active'=> 1,
           'customer_id'=> 1,
       ]);
       offices::create([
        'name'=> 'Puerto Norte',
        'active'=> 1,
        'customer_id'=> 1,
    ]);
    offices::create([
        'name'=> 'Pichincha',
        'active'=> 1,
        'customer_id'=> 1,
    ]);
    offices::create([
        'name'=> 'Oroño',
        'active'=> 1,
        'customer_id'=> 1,
    ]);
    }
}
